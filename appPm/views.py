from django.shortcuts import render
from django.http import HttpResponse
from django.http import QueryDict
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.core.files.storage import FileSystemStorage
import json
import base64
import cv2
import numpy as np
import pywt
from matplotlib import pyplot as plt



# Create your views here.
def index(request):
    data = {}
    data['home'] = 'active'
    return render(request, 'index.html', {'menu': data, })

@csrf_exempt
def point_operators(request):

    if request.method == 'POST':
        if request.is_ajax():
            Diccionario= dict(request.POST.lists())
            option=Diccionario['option'][0]
            aux=Diccionario['aux'][0]
            file=request.FILES['file']
            fs=FileSystemStorage()
            filename=fs.save(file.name,file)
            print(file.name)
            print("aux:",aux)
            print("option",option)
            with open(file.name, "rb") as image_file:
                #encoded_string = base64.b64encode(image_file.read())
                if(option=="1"):
                    print("option!!!!!!!!!!!!!!!!!!!!!!!!")
                    encoded_string=thresholding(file.name,aux)
                    print("xxxxxxxxx")
    return HttpResponse(encoded_string)

def thresholding(imgName=None,aux=0):
    img = cv2.imread(imgName, cv2.IMREAD_GRAYSCALE)
    imageWidth = img.shape[1]
    imageHeight = img.shape[0]
    xPos, yPos = 0, 0
    x = 0
    y = 0
    #hist = cv2.calcHist([img], [0], None, [256], [0, 256])
    for y in range(imageWidth):
        for x in range(imageHeight):
            if (img[x, y] < int(aux)):
                img[x, y] = 0
            if (img[x, y] > int(aux)):
                img[x, y] = 250
    nombre_img="Tresholding.jpg"
    cv2.imwrite(nombre_img, img)
    with open(nombre_img, "rb") as image_file:
        encoded_string =base64.b64encode(image_file.read())
    print(encoded_string)
    return(encoded_string)


def scalar(request):
    data = {}
    data['home'] = 'active'
    return render(request, 'aritmetic/scalar.html', {'menu': data, })

@csrf_exempt
def scalar_operators(request):
    if request.method == 'POST':
        if request.is_ajax():
            Diccionario= dict(request.POST.lists())
            option=Diccionario['option'][0]
            aux=Diccionario['aux'][0]
            file=request.FILES['file']
            fs=FileSystemStorage()
            filename=fs.save(file.name,file)
            print(file.name)
            print("aux:",aux)
            print("option",option)
            encoded_string=scalar_operator_result(file.name,aux,option)
    return HttpResponse(encoded_string)

def scalar_operator_result(imgName=None, aux=0,option=1):
    img = cv2.imread(imgName, cv2.IMREAD_GRAYSCALE)
    imageWidth = img.shape[1]
    imageHeight = img.shape[0]
    xPos, yPos = 0, 0
    x = 0
    y = 0
    if(option=="1"):
        for y in range(imageWidth):
            for x in range(imageHeight):
                img[x, y] = img[x,y]+int(aux)
    if (option == "2"):
        for y in range(imageWidth):
            for x in range(imageHeight):
                img[x, y] = img[x, y]-int(aux)
    if (option == "3"):
        for y in range(imageWidth):
            for x in range(imageHeight):
                img[x, y] = img[x, y]* int(aux)
    if (option == "4"):
        for y in range(imageWidth):
            for x in range(imageHeight):
                img[x, y] = int(img[x, y] /int(aux))

    nombre_img = "Scalar.jpg"
    cv2.imwrite(nombre_img, img)
    with open(nombre_img, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    #print(encoded_string)
    return (encoded_string)


def logicalScalar(request):
    data = {}
    data['home'] = 'active'
    return render(request, 'logical/scalar.html', {'menu': data, })

@csrf_exempt
def logical_scalar_operators(request):
    if request.method == 'POST':
        if request.is_ajax():
            Diccionario= dict(request.POST.lists())
            option=Diccionario['option'][0]
            aux=Diccionario['aux'][0]
            file=request.FILES['file']
            fs=FileSystemStorage()
            filename=fs.save(file.name,file)
            print(file.name)
            print("aux:",aux)
            print("option",option)
            encoded_string=logical_scalar_operator_result(file.name,aux,option)
    return HttpResponse(encoded_string)

def logical_scalar_operator_result(imgName=None, aux=0,option=1):
    img = cv2.imread(imgName, cv2.IMREAD_GRAYSCALE)
    imageWidth = img.shape[1]
    imageHeight = img.shape[0]
    xPos, yPos = 0, 0
    x = 0
    y = 0
    if(option=="1"):
        for y in range(imageWidth):
            for x in range(imageHeight):
                img[x, y] = 255-img[x,y]
    if (option == "2"):
        for y in range(imageWidth):
            for x in range(imageHeight):
                img[x, y] = img[x, y]&int(aux)
    if (option == "3"):
        for y in range(imageWidth):
            for x in range(imageHeight):
                img[x, y] = img[x, y]| int(aux)

    nombre_img = "logicalScalar.jpg"
    cv2.imwrite(nombre_img, img)
    with open(nombre_img, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    #print(encoded_string)
    return (encoded_string)

def morphology(request):
    data = {}
    data['home'] = 'active'
    return render(request, 'morphology/morphology.html', {'menu': data, })

@csrf_exempt
def morphology_operators(request):
    if request.method == 'POST':
        if request.is_ajax():
            Diccionario= dict(request.POST.lists())
            option=Diccionario['option'][0]
            aux=Diccionario['aux'][0]
            file=request.FILES['file']
            fs=FileSystemStorage()
            filename=fs.save(file.name,file)
            print(file.name)
            print("aux:",aux)
            print("option",option)
            encoded_string=morphology_operator_result(file.name,aux,option)
    return HttpResponse(encoded_string)

def morphology_operator_result(imgName=None, aux=0,option=1):
    img = cv2.imread(imgName, cv2.IMREAD_GRAYSCALE)
    imageWidth = img.shape[1]
    imageHeight = img.shape[0]
    xPos, yPos = 0, 0
    nombre_morph="Morphology.jpg"
    kernel = np.ones((5, 5), np.uint8)
    if(option=="1"):
        erosion = cv2.erode(img, kernel, iterations=1)
        cv2.imwrite(nombre_morph,erosion)
    if (option == "2"):
        dilation = cv2.dilate(img, kernel, iterations=1)
        cv2.imwrite(nombre_morph, dilation)
    if (option == "3"):
        opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
        cv2.imwrite(nombre_morph, opening)
    if (option == "4"):
        closing = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)
        cv2.imwrite(nombre_morph, closing)

    with open(nombre_morph, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    #print(encoded_string)
    return (encoded_string)

def filters(request):
    data = {}
    data['home'] = 'active'
    return render(request, 'filters/filters.html', {'menu': data, })
@csrf_exempt
def filters_operators(request):
    if request.method == 'POST':
        if request.is_ajax():
            Diccionario= dict(request.POST.lists())
            option=Diccionario['option'][0]
            aux=Diccionario['aux'][0]
            file=request.FILES['file']
            fs=FileSystemStorage()
            filename=fs.save(file.name,file)
            print(file.name)
            print("aux:",aux)
            print("option",option)
            encoded_string=filters_operators_result(file.name,aux,option)
    return HttpResponse(encoded_string)

def filters_operators_result(imgName=None, aux=0,option=1):
    img = cv2.imread(imgName, cv2.IMREAD_GRAYSCALE)
    imageWidth = img.shape[1]
    imageHeight = img.shape[0]
    xPos, yPos = 0, 0
    nombre_morph="filters.jpg"
    kernel = np.ones((5, 5), np.uint8)
    if(option=="1"):
        blur = cv2.blur(img, (5, 5))
        cv2.imwrite(nombre_morph,blur)
    if (option == "2"):
        median = cv2.medianBlur(img, 5)
        cv2.imwrite(nombre_morph, median)
    if (option == "3"):
        gaussian = cv2.GaussianBlur(img, (5, 5), 0)
        cv2.imwrite(nombre_morph, gaussian)

    with open(nombre_morph, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    #print(encoded_string)
    return (encoded_string)

def bordes(request):
    data = {}
    data['home'] = 'active'
    return render(request, 'bordes/bordes.html', {'menu': data, })

@csrf_exempt
def bordes_operators(request):
    if request.method == 'POST':
        if request.is_ajax():
            Diccionario= dict(request.POST.lists())
            option=Diccionario['option'][0]
            aux=Diccionario['aux'][0]
            file=request.FILES['file']
            fs=FileSystemStorage()
            filename=fs.save(file.name,file)
            print(file.name)
            print("aux:",aux)
            print("option",option)
            encoded_string=bordes_operators_result(file.name,aux,option)
    return HttpResponse(encoded_string)

def bordes_operators_result(imgName=None, aux=0,option=1):
    img = cv2.imread(imgName, cv2.IMREAD_GRAYSCALE)
    imageWidth = img.shape[1]
    imageHeight = img.shape[0]
    xPos, yPos = 0, 0
    nombre_morph="Bordes.jpg"
    kernel = np.ones((5, 5), np.uint8)
    if(option=="1"):
        canny=cv2.Canny(img,100,200)
        cv2.imwrite(nombre_morph,canny)


    with open(nombre_morph, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    #print(encoded_string)
    return (encoded_string)

def aritmetic(request):
    data = {}
    data['home'] = 'active'
    return render(request, 'aritmetic/Aritmetic.html', {'menu': data, })

@csrf_exempt
def aritmetic_operators(request):
    if request.method == 'POST':
        if request.is_ajax():
            Diccionario= dict(request.POST.lists())
            option=Diccionario['option'][0]
            aux=Diccionario['aux'][0]
            file=request.FILES['file']
            file2=request.FILES['file2']
            fs=FileSystemStorage()
            filename=fs.save(file.name,file)
            filename2 = fs.save(file2.name, file2)
            print(file.name)
            print("aux:",aux)
            print("option",option)
            encoded_string=aritmetic_operators_result(file.name,file2.name,option)
    return HttpResponse(encoded_string)

def aritmetic_operators_result(imgName=None, imgName2=0,option=1):
    img = cv2.imread(imgName, cv2.IMREAD_GRAYSCALE)
    img2 = cv2.imread(imgName2, cv2.IMREAD_GRAYSCALE)
    imageWidth = img.shape[1]
    imageHeight = img.shape[0]
    xPos, yPos = 0, 0
    nombre_morph="Aritmeticimages.jpg"

    if (option == "1"):
        for y in range(imageWidth):
            for x in range(imageHeight):
                img[x, y] =img[x, y]+img2[x,y]
    if (option == "2"):
        for y in range(imageWidth):
            for x in range(imageHeight):
                img[x, y] =img[x, y]-img2[x,y]
    if (option == "3"):
        for y in range(imageWidth):
            for x in range(imageHeight):
                img[x, y] =img[x, y]*img2[x,y]

    if (option == "4"):
        for y in range(imageWidth):
            for x in range(imageHeight):
                img[x, y] =int(img[x, y]/img2[x,y])

    cv2.imwrite(nombre_morph, img)
    with open(nombre_morph, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    #print(encoded_string)
    return (encoded_string)

def sift(request):
    data = {}
    data['home'] = 'active'
    return render(request, 'sift/sift.html', {'menu': data, })

@csrf_exempt
def sift_operators(request):
    if request.method == 'POST':
        if request.is_ajax():
            Diccionario= dict(request.POST.lists())
            option=Diccionario['option'][0]
            aux=Diccionario['aux'][0]
            file=request.FILES['file']
            fs=FileSystemStorage()
            filename=fs.save(file.name,file)
            print(file.name)
            print("aux:",aux)
            print("option",option)
            encoded_string=sift_operators_result(file.name,aux,option)
    return HttpResponse(encoded_string)

def sift_operators_result(imgName=None, aux=0,option=1):
    img = cv2.imread(imgName)

    imageWidth = img.shape[1]
    imageHeight = img.shape[0]
    xPos, yPos = 0, 0
    nombre_morph="sift.jpg"

    if (option == "1"):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        sift = cv2.xfeatures2d.SIFT_create()
        kp = sift.detect(gray, None)
        img = cv2.drawKeypoints(gray, kp, 1)

        cv2.imwrite(nombre_morph, img)
    with open(nombre_morph, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    #print(encoded_string)
    return (encoded_string)


def wavelets(request):
    data = {}
    data['home'] = 'active'
    return render(request, 'wavelets/wavelets.html', {'menu': data, })

@csrf_exempt
def wavelets_operators(request):
    if request.method == 'POST':
        if request.is_ajax():
            Diccionario= dict(request.POST.lists())
            option=Diccionario['option'][0]
            aux=Diccionario['aux'][0]
            file=request.FILES['file']
            fs=FileSystemStorage()
            filename=fs.save(file.name,file)
            print(file.name)
            print("aux:",aux)
            print("option",option)
            encoded_string=wavelets_operators_result(file.name,aux,option)
    return HttpResponse(encoded_string)

def wavelets_operators_result(imgName=None, aux=0,option=1):

    nombre_morph="wavelets.jpg"
    mode = 'db1'
    level = 6

    if (option == "1"):
        imArray = cv2.imread(imgName)
        # Datatype conversions
        # convert to grayscale
        imArray = cv2.cvtColor(imArray, cv2.COLOR_RGB2GRAY)
        # convert to float
        imArray = np.float32(imArray)
        imArray /= 255;
        # compute coefficients
        coeffs = pywt.wavedec2(imArray, mode, level=level)

        # Process Coefficients
        coeffs_H = list(coeffs)
        coeffs_H[0] *= 0;

        # reconstruction
        imArray_H = pywt.waverec2(coeffs_H, mode);
        imArray_H *= 255;
        imArray_H = np.uint8(imArray_H)

        cv2.imwrite(nombre_morph, imArray_H)
    with open(nombre_morph, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    #print(encoded_string)
    return (encoded_string)

def fourier(request):
    data = {}
    data['home'] = 'active'
    return render(request, 'fourier/fourier.html', {'menu': data, })

@csrf_exempt
def fourier_operators(request):
    if request.method == 'POST':
        if request.is_ajax():
            Diccionario= dict(request.POST.lists())
            option=Diccionario['option'][0]
            aux=Diccionario['aux'][0]
            file=request.FILES['file']
            fs=FileSystemStorage()
            filename=fs.save(file.name,file)
            print(file.name)
            print("aux:",aux)
            print("option",option)
            encoded_string=fourier_operators_result(file.name,aux,option)
    return HttpResponse(encoded_string)

def fourier_operators_result(imgName=None, aux=0,option=1):

    nombre_morph="fourier.jpg"


    img = cv2.imread(imgName, 0)
    if (option == "1"):

        f = np.fft.fft2(img)
        fshift = np.fft.fftshift(f)
        magnitude_spectrum = 20 * np.log(np.abs(fshift))
        plt.imshow(magnitude_spectrum, cmap='gray')
        plt.savefig('fourier.jpg')


    with open(nombre_morph, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    #print(encoded_string)
    return (encoded_string)


def selectiveSearch(request):
    data = {}
    data['home'] = 'active'
    return render(request, 'selectiveSearch/selectiveSearch.html', {'menu': data, })

@csrf_exempt
def selectiveSearch_operators(request):
    if request.method == 'POST':
        if request.is_ajax():
            Diccionario= dict(request.POST.lists())
            option=Diccionario['option'][0]
            aux=Diccionario['aux'][0]
            file=request.FILES['file']
            fs=FileSystemStorage()
            filename=fs.save(file.name,file)
            print(file.name)
            print("aux:",aux)
            print("option",option)
            encoded_string=selectiveSearch_operators_result(file.name,aux,option)
    return HttpResponse(encoded_string)

def selectiveSearch_operators_result(imgName=None, aux=0,option=1):

    nombre_morph="selective.jpg"
    im = cv2.imread(imgName)

    if (option == "1"):

        option = 'q'
        newHeight = 200
        newWidth = int(im.shape[1] * 200 / im.shape[0])
        im = cv2.resize(im, (newWidth, newHeight))

        # create Selective Search Segmentation Object using default parameters
        ss = cv2.ximgproc.segmentation.createSelectiveSearchSegmentation()

        # set input image on which we will run segmentation
        ss.setBaseImage(im)

        # Switch to fast but low recall Selective Search method
        if (option== 'f'):
            ss.switchToSelectiveSearchFast()

        # Switch to high recall but slow Selective Search method
        elif (option == 'q'):
            ss.switchToSelectiveSearchQuality()
        # if argument is neither f nor q print help message


        # run selective search segmentation on input image
        rects = ss.process()
        print('Total Number of Region Proposals: {}'.format(len(rects)))

        # number of region proposals to show
        numShowRects = 100
        # increment to increase/decrease total number
        # of reason proposals to be shown
        increment = 50


        # create a copy of original image
        imOut = im.copy()

        # itereate over all the region proposals
        for i, rect in enumerate(rects):
            # draw rectangle for region proposal till numShowRects
            if (i < numShowRects):
                x, y, w, h = rect
                cv2.rectangle(imOut, (x, y), (x + w, y + h), (0, 255, 0), 1, cv2.LINE_AA)
            else:
                break


        cv2.imwrite(nombre_morph, imOut)


    with open(nombre_morph, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    #print(encoded_string)
    return (encoded_string)
