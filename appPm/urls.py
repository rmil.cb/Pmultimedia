from django.conf.urls import url,include
from django.contrib.auth.views import logout
from . import views

urlpatterns = [

    url(r'^index/', views.index, name='index'),
    url(r'^scalar/$', views.scalar, name='scalar'),
    url(r'^logicalScalar/$', views.logicalScalar, name='Logical scalar'),
    url(r'^morphology/$', views.morphology, name='Morphology'),
    url(r'^filters/$', views.filters, name='Filters'),
    url(r'^bordes/$', views.bordes, name='Filters'),
    url(r'^aritmetic/$', views.aritmetic, name='Filters'),
    url(r'^sift/$', views.sift, name='sift'),
    url(r'^wavelets/$', views.wavelets, name='wavelets'),
    url(r'^fourier/$', views.fourier, name='fourier'),
    url(r'^selectiveSearch/$', views.selectiveSearch, name='selectiveSearch'),
    #url(r'^login/', include('django.contrib.auth.urls'), {'extra_context': {'next': '/sistemacomercial/index/'}}),
    url(r'^pointOperators/$',views.point_operators,name='point_operators'),
    url(r'^scalarOperators/$',views.scalar_operators,name='scalar_operators'),
    url(r'^logicalScalarOperators/$', views.logical_scalar_operators, name='Logical scalar'),
    url(r'^morphologyOperators/$', views.morphology_operators, name='morphologyOperatorss'),
    url(r'^filtersOperators/$', views.filters_operators, name='FiltersOperatorss'),
    url(r'^bordesOperators/$', views.bordes_operators, name='FiltersOperatorss'),
    url(r'^aritmeticOperators/$', views.aritmetic_operators, name='Filters'),
    url(r'^siftOperators/$', views.sift_operators, name='sift'),
    url(r'^waveletsOperators/$', views.wavelets_operators, name='wavelets'),
    url(r'^fourierOperators/$', views.fourier_operators, name='fourier'),
    url(r'^selectiveSearchOperators/$', views.selectiveSearch_operators, name='selectiveSearch'),

]
